# open-cuts-ansible

This repository contains [ansible playbooks](https://www.ansible.com/) for setting up and maintaining [OPEN-CUTS, the open crowdsourced user testing suite](https://www.open-cuts.org).

## Installation

1. Create an ssh key by running `ssh-keygen -t ed25519 -C opencuts -f ~/.ssh/id_ed25519_opencuts -N ""`.
2. Set up a Linux server somewhere ([Digital Ocean affiliate link, wink wink nudge nudge](https://m.do.co/c/48920e664c08)). Ubuntu 20.04 is recommended, but others might work as well.
3. Obtain a domain name and create an `AAAA` (or `A` if you're feeling whimsically 1995) dns record to point to your server's IP address.
4. Enable root-access over ssh to your server by adding your previously generated publickey (`~/.ssh/id_ed25519_opencuts.pub`) to the server's `/root/.ssh/authorized_keys` directory.
5. Configure access to your server in your local `~/.ssh/config` using the alias `opencuts`. Example:

```bash
Host opencuts
  HostName your-domain.open-cuts.org # put your domain name here
  User opencuts
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/id_ed25519_opencuts.pub
```

6. Configure OPEN-CUTS by editing the `playbooks/config.yml`:

```yml
mongodb_user: "your-mongodb-user" # be creative!
mongodb_pass: "your-mongodb-pass" # choose a secure password
seed_user: Admin # used to create the admin user
seed_email: your-email@open-cuts.org # used to create the admin user
webmaster_email: your-email@open-cuts.org # used as a domain contact
website_title: My OPEN-CUTS instance # will be used accorss the website
website_author: Johannah Sprinz # will be shown in the footer
website_author_link: https://spri.nz # will be linked in the footer
http_host: "your-domain.open-cuts.org" # put your domain name here
smtp_config: "'{\"host\":\"mail.example.org\", \"port\":465, \"secure\":true, \"auth\":{\"user\":\"your-email@example.org\", \"pass\":\"horse-battery-staple\"}}'" # smtp auth for nodemailer, see https://nodemailer.com/about/#tldr
secret: "your-random-secret" # extra entropy for hashing, smash the keyboard like your life depends on it
copy_local_key: "{{ lookup('file', lookup('env','HOME') + '/.ssh/id_ed25519_opencuts.pub') }}" # point this to an ssh key
```

7. Run `ansible-playbook playbooks/setup.yml -u root` for initial prerequistes. This will install the required dependencies and create an `opencuts` user to work with. Finally, the server will reboot.
8. Run `ansible-playbook playbooks/main.yml -u opencuts` to set up OPEN-CUTS on your server.
9. Open your domain in your favorite webbrowser and marvel at the endless possibilities.
10. That's it! Set up your software to be tested with OPEN-CUTS and get your community testers to report their results. Crowdsourced user testing is easy!

## License

Original development by [Johannah Sprinz](https://spri.nz). Copyright (C) 2020-2022 [UBports Foundation](https://ubports.com).

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
